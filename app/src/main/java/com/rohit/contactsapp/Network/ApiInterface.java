package com.rohit.contactsapp.Network;

import com.rohit.contactsapp.models.EmailsResponses.EmailListResponse;
import com.rohit.contactsapp.models.LoginResponsess.LoginResponse;
import java.util.Map;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("rest.php")
    Call<LoginResponse> logIn(
            @FieldMap Map<String,Object> data
    );

    @FormUrlEncoded
    @POST("emails.php")
    Call<EmailListResponse> fetchEmails(
            @FieldMap Map<String,Object> data
    );

    @POST("emails.php")
    Call<EmailListResponse> fetchEmails2(
    );

//    @GET("modules.php")
//    Call<Models> getModels();
}
