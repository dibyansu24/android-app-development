package com.rohit.contactsapp.models.EmailsResponses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EmailListResponse {


    @Expose
    @SerializedName("relationship_list")
    private List<String> relationshipList;
    @Expose
    @SerializedName("entry_list")
    private List<EntryList> entryList;
    @Expose
    @SerializedName("next_offset")
    private int nextOffset;
    @Expose
    @SerializedName("total_count")
    private String totalCount;
    @Expose
    @SerializedName("result_count")
    private int resultCount;

    public List<String> getRelationshipList() {
        return relationshipList;
    }

    public void setRelationshipList(List<String> relationshipList) {
        this.relationshipList = relationshipList;
    }

    public List<EntryList> getEntryList() {
        return entryList;
    }

    public void setEntryList(List<EntryList> entryList) {
        this.entryList = entryList;
    }

    public int getNextOffset() {
        return nextOffset;
    }

    public void setNextOffset(int nextOffset) {
        this.nextOffset = nextOffset;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public int getResultCount() {
        return resultCount;
    }

    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }


}
