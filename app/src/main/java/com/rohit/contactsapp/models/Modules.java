package com.rohit.contactsapp.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Modules {
    @SerializedName("module_key")
    @Expose
    private String module_key;

    @SerializedName("module_label")
    @Expose
    private String module_label;

    @SerializedName("favorite_enabled")
    @Expose
    private Boolean favorite_enabled;

    public Modules(String module_key, String module_label, Boolean favorite_enabled) {
        this.module_key = module_key;
        this.module_label = module_label;
        this.favorite_enabled = favorite_enabled;
    }

    public String getModule_key() {
        return module_key;
    }

    public void setModule_key(String module_key) {
        this.module_key = module_key;
    }

    public String getModule_label() {
        return module_label;
    }

    public void setModule_label(String module_label) {
        this.module_label = module_label;
    }

    public Boolean getFavorite_enabled() {
        return favorite_enabled;
    }

    public void setFavorite_enabled(Boolean favorite_enabled) {
        this.favorite_enabled = favorite_enabled;
    }
}
