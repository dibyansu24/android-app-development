
package com.rohit.contactsapp.models.LoginResponsess;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserIsAdmin {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("value")
    @Expose
    private Boolean value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getValue() {
        return value;
    }

    public void setValue(Boolean value) {
        this.value = value;
    }

}
