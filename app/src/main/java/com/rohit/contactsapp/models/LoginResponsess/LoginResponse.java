
package com.rohit.contactsapp.models.LoginResponsess;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("module_name")
    @Expose
    private String moduleName;
    @SerializedName("name_value_list")
    @Expose
    private NameValueList nameValueList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public NameValueList getNameValueList() {
        return nameValueList;
    }

    public void setNameValueList(NameValueList nameValueList) {
        this.nameValueList = nameValueList;
    }

}
