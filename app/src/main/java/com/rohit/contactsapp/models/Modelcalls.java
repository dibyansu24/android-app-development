package com.rohit.contactsapp.models;

import android.graphics.Bitmap;

public class Modelcalls {
    private String name, number,duration,date,callLogId;
    private byte[] image;


//    public Modelcalls(String number, String duration, String date,String id) {
//        this.callLogId = id;
//      //  this.name = name;
//        this.number = number;
//        this.duration = duration;
//        this.date = date;
//    }


    public Modelcalls() {
    }

    public Modelcalls(String name, String number, String duration, String date, String callLogId, byte[] image) {
        this.name = name;
        this.number = number;
        this.duration = duration;
        this.date = date;
        this.callLogId = callLogId;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCallLogId() {
        return callLogId;
    }

    public void setCallLogId(String callLogId) {
        this.callLogId = callLogId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}

