package com.rohit.contactsapp.models;

public class Modelsfordashboard {
    private String date;
    private String endDate;
    private int progress;

    public Modelsfordashboard(String date, String endDate, int progress) {
        this.date = date;
        this.endDate = endDate;
        this.progress = progress;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }
}
