package com.rohit.contactsapp.models.EmailsResponses;

public class RequestBodyCallList {

    private String input_type;
    private String response_type;
    private String method;
///    private String rest_data;
    private String session;
    private String module_name;
    private String query;
    private String assigned_user_id;
    private String order_by;
    private int offset;
    private String max_results;
    private String deleted;
    private String Favorites;

    public RequestBodyCallList(String input_type, String response_type, String method, String session, String module_name, String query, String assigned_user_id, String order_by, int offset, String max_results, String deleted, String favorites) {
        this.input_type = input_type;
        this.response_type = response_type;
        this.method = method;
        this.session = session;
        this.module_name = module_name;
        this.query = query;
        this.assigned_user_id = assigned_user_id;
        this.order_by = order_by;
        this.offset = offset;
        this.max_results = max_results;
        this.deleted = deleted;
        Favorites = favorites;
    }

    public RequestBodyCallList() {
    }

    public String getInput_type() {
        return input_type;
    }

    public void setInput_type(String input_type) {
        this.input_type = input_type;
    }

    public String getResponse_type() {
        return response_type;
    }

    public void setResponse_type(String response_type) {
        this.response_type = response_type;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }


    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getModule_name() {
        return module_name;
    }

    public void setModule_name(String module_name) {
        this.module_name = module_name;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getAssigned_user_id() {
        return assigned_user_id;
    }

    public void setAssigned_user_id(String assigned_user_id) {
        this.assigned_user_id = assigned_user_id;
    }

    public String getOrder_by() {
        return order_by;
    }

    public void setOrder_by(String order_by) {
        this.order_by = order_by;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getMax_results() {
        return max_results;
    }

    public void setMax_results(String max_results) {
        this.max_results = max_results;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getFavorites() {
        return Favorites;
    }

    public void setFavorites(String favorites) {
        Favorites = favorites;
    }
}


