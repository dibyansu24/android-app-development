package com.rohit.contactsapp.fragments;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.rohit.contactsapp.R;
import com.rohit.contactsapp.adapters.ContactListAdapter;
import com.rohit.contactsapp.models.EmailsResponses.ContactModel;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class FragmentContact extends Fragment  {
    // auther dhanashri shisode
    ArrayList<ContactModel> modelList;
    private Context context;
    ProgressDialog progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.retrive_contacts_fragment, container, false);
        RecyclerView recyclerView = v.findViewById(R.id.rv_contact);
        recyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        prepareContactList();
        ContactListAdapter contactListAdapter = new ContactListAdapter(v.getContext(), modelList);
        recyclerView.setAdapter(contactListAdapter);
        recyclerView.setHasFixedSize(true);

        return v;
    }
    private void prepareContactList() {
        modelList = getAllContacts();
    }
    private ArrayList getAllContacts() {
        ContentResolver cr = getActivity().getContentResolver();
        modelList = new ArrayList<ContactModel>();

        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI,null,null,null,null);
        if ((cursor != null ?  cursor.getCount() : 0) > 0) {
            while (cursor.moveToNext()) {
                ContactModel contactInfo = new ContactModel();
                String id  = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                contactInfo.setDisplayName(name);
                contactInfo.setContactId(id);

                if (cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))>0){
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new  String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contactInfo.setPhoneNumber(phoneNo);

                    }
                    pCur.close();

                    Bitmap photo = null;

                    try {
                        InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(this.getActivity().getContentResolver(),
                                ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(id)));

                        if (inputStream != null) {
                            photo = BitmapFactory.decodeStream(inputStream);
                            contactInfo.setImage(photo);
                        }
                        if (inputStream != null) inputStream.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    modelList.add(contactInfo);
                }
            }
        }
        if (cursor != null){
            cursor.close();
        }
        return modelList;
    }

}


//    ArrayList<ContactModel> modelList;
//
//    public FragmentContact(ArrayList<ContactModel> contactList) {
//        modelList = contactList;
//    }
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        RecyclerView rv = new RecyclerView(getContext());
//        rv.setLayoutManager(new LinearLayoutManager(getContext()));
//        rv.setAdapter(new SimpleRVAdapter(modelList));
//        return rv;
//    }
//
//    public class SimpleRVAdapter extends RecyclerView.Adapter<SimpleViewHolder> {
//        private List<ContactModel> dataSource = new ArrayList<>();
//        public SimpleRVAdapter(List<ContactModel> dataArgs){
//            dataSource = dataArgs;
//        }
//
//        @Override
//        public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            View view = new TextView(parent.getContext());
//            SimpleViewHolder viewHolder = new SimpleViewHolder(view);
//            return viewHolder;
//        }
//
//        @Override
//        public void onBindViewHolder(SimpleViewHolder holder, int position) {
//            final ContactModel myListData = modelList.get(position);
//
//            // holder.textView.setText(dataSource.get(position).getDisplayName());
//        }
//
//        @Override
//        public int getItemCount() {
//            return dataSource.size();
//        }
//    }
//
//    public static class SimpleViewHolder extends RecyclerView.ViewHolder{
//        public TextView textView;
//        public SimpleViewHolder(View itemView) {
//            super(itemView);
//            textView = (TextView) itemView;
//        }
//    }


