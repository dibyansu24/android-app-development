package com.rohit.contactsapp.fragments;


import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.CallLog;
import android.content.Context;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.rohit.contactsapp.R;
import com.rohit.contactsapp.adapters.CallsRvAdapter;
import com.rohit.contactsapp.models.Modelcalls;
import java.util.ArrayList;
import java.util.List;

public class FragmentCalls extends Fragment {

   private View v;
    private RecyclerView recyclerView;
    ArrayList<Modelcalls> modelList ;
    Context mContext;
    public FragmentCalls(){

    }

//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState){
//        v= inflater.inflate(R.layout.frag_calls,container,false);
//        recyclerView=v.findViewById(R.id.rv_calls);
//        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext());
//        RecyclerView.LayoutManager layoutManager=linearLayoutManager;
//        recyclerView.setLayoutManager(layoutManager);
//        CallsRvAdapter adapter=new CallsRvAdapter(getContext(),getCallLogs());
//        recyclerView.setAdapter(adapter);
//        return v;
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_calls, container, false);
        recyclerView = v.findViewById(R.id.rv_calls);
        recyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        getCallLogs();
        CallsRvAdapter adapter = new CallsRvAdapter(v.getContext(), modelList);
        recyclerView.setAdapter(adapter);

        //  RecyclerView.ItemDecoration decoration = new DividerItemDecoration(FragmentCalls.this,DividerItemDecoration.VERTICAL);
        return v;
    }

    private ArrayList getCallLogs() {
//        modelList = new ArrayList<Modelcalls>();
//        Context mContext = getContext();

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_CALL_LOG}, 1);
        }


        Cursor cursor = getActivity().getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
        if ((cursor != null ? cursor.getCount() : 0) > 0) {
            while (cursor.moveToNext()) {
                Modelcalls callLogsInfo = new Modelcalls();
                String id = cursor.getString(cursor.getColumnIndex(CallLog.Calls._ID));
                String number = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
                String name = cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_NAME));
                String date = cursor.getString(cursor.getColumnIndex(CallLog.Calls.DATE));


                callLogsInfo.setCallLogId(id);
                callLogsInfo.setNumber(number);
                callLogsInfo.setName(name);
                callLogsInfo.setDate(date);

             int imageDataRow = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.CACHED_PHOTO_ID));
                Cursor c = mContext.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{ ContactsContract.CommonDataKinds.Photo.PHOTO }, ContactsContract.Data._ID + "=?", new String[]{ Integer.toString(imageDataRow)}, null);
                byte[] imageBytes = null;
                if (c != null) {
                    if (c.moveToFirst()) {
                        imageBytes = c.getBlob(0);
                    }
                    c.close();
                }
                callLogsInfo.setImage(imageBytes);
                modelList.add(callLogsInfo);

            }
        }
                return modelList;
            }
        }



