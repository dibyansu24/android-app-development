package com.rohit.contactsapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rohit.contactsapp.Activities.ContactsDetailsActivity;
import com.rohit.contactsapp.R;
import com.rohit.contactsapp.models.EmailsResponses.ContactModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

//auther dhanashri shisode
public class ContactListAdapter extends  RecyclerView.Adapter<ContactListAdapter.ViewHolder>{
    ArrayList<ContactModel> modelList = new ArrayList<ContactModel>();
    Context context;

    public ContactListAdapter(Context context,ArrayList<ContactModel> contactList) {
        this.context = context;
        this.modelList = contactList;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

     String getInitials(String name) {
         String initials = "";
        if (name.length() == 0)
            return initials;

         char initials1 = Character.toUpperCase(name.charAt(0));

        for (int i = 1; i < name.length() - 1; i++)
            if (name.charAt(i) == ' ') {
                initials = initials1 + " " + Character.toUpperCase(name.charAt(i + 1));
                return initials;
            }
        return  initials1 + "";
    }

    @Override
    public void onBindViewHolder(@NonNull final ContactListAdapter.ViewHolder holder, final int position ) {
        final ContactModel contact = modelList.get(position);
        holder.textView.setText(contact.getDisplayName());
        if (contact.getImage() == null) {
            holder.initialsTextView.setText(getInitials(contact.getDisplayName()));
            holder.imageView.setVisibility(View.INVISIBLE);
            holder.initialsTextView.setVisibility(View.VISIBLE);
        } else {
            holder.imageView.setVisibility(View.VISIBLE);
            holder.initialsTextView.setVisibility(View.INVISIBLE);
            holder.imageView.setImageBitmap(contact.getImage());
        }

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContactModel contact = modelList.get(position);
                context = view.getContext();
                String contactNumber = contact.getPhoneNumber();
                String contactName = contact.getDisplayName();
                Bitmap image = contact.getImage();
                Intent intent=new Intent(context, ContactsDetailsActivity.class);

                intent.putExtra("contactPerson", contactName);
                intent.putExtra("contactNo",contactNumber);
                intent.putExtra("contactImage",image);
                // intent.putExtra("contactImage",image);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelList.size() ;
    }

    public static  class ViewHolder extends RecyclerView.ViewHolder {
         public CircleImageView imageView;
        public TextView textView,initialsTextView;

        public RelativeLayout relativeLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.list_desc);
            imageView = itemView.findViewById(R.id.profile_image);
            initialsTextView = itemView.findViewById(R.id.placeholder);
            relativeLayout = itemView.findViewById(R.id.relativelayout);
        }
    }
}


