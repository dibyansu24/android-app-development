package com.rohit.contactsapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.rohit.contactsapp.R;

public class SliderAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflaterl;


    public SliderAdapter(Context context) {
        this.context = context;
    }

    public int[] slide_images = {
           /* R.drawable.use,
            R.drawable.sale,
            R.drawable.build,
            R.drawable.grow,
            R.drawable.agreement,

*/
        //  R.drawable.use,
          R.drawable.build,
            R.drawable.use,
            R.drawable.sale,
            R.drawable.agreement
    };


    @Override
    public int getCount() {
        return slide_images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflaterl = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflaterl.inflate(R.layout.slide_layout,container,false);

        ImageView slideImageView = view.findViewById(R.id.images_slider);
        slideImageView.setImageResource(slide_images[position]);
        container.addView(view);
        return view;
    }

    public void destroyItem(ViewGroup container, int position, Object object){
        container.removeView((RelativeLayout)object);
    }

}
