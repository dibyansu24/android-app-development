package com.rohit.contactsapp.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.rohit.contactsapp.fragments.FragmentCRMCalls;
import com.rohit.contactsapp.fragments.FragmentCalls;
import com.rohit.contactsapp.fragments.FragmentContact;
import com.rohit.contactsapp.fragments.FragmentCrmContacts;

import java.util.ArrayList;
import java.util.List;


public class ViewPagerAdapter extends FragmentPagerAdapter {

    Context myContext;
    int totalTabs;

    public ViewPagerAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FragmentCalls callLogFragment = new FragmentCalls();
                return callLogFragment;
            default:
                FragmentCRMCalls fragmentCrmCalls = new FragmentCRMCalls();
                return fragmentCrmCalls;
        }
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}