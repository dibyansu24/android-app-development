package com.rohit.contactsapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.rohit.contactsapp.R;
import com.rohit.contactsapp.models.Modelcalls;
import java.util.Date;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;


public class CallsRvAdapter extends RecyclerView.Adapter<CallsRvAdapter.ViewHolder> {
    private LayoutInflater layoutInflater;
    private Context mContext;
    private List<Modelcalls> mlistCalls;

    public CallsRvAdapter(Context context, List<Modelcalls> listcalls) {
        mContext = context;
        mlistCalls = listcalls;
    }

    public CallsRvAdapter(Context context) {
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.item_call, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final TextView name, duration, date;
        final CircleImageView imageView;
        name = holder.name;
        duration = holder.duration;
        date = holder.date;
        imageView = holder.callLogImage;

        name.setText(mlistCalls.get(position).getNumber());
        duration.setText(mlistCalls.get(position).getName());

        String dateLong = mlistCalls.get(position).getDate();
        long millisecond = Long.parseLong(dateLong);
        String dateString = DateFormat.format("MM/dd/yyyy HH:mm", new Date(millisecond)).toString();
        date.setText(dateString);

        byte[] temp = mlistCalls.get(position).getImage();
        if (temp != null) {
            Bitmap photo = BitmapFactory.decodeByteArray(temp, 0, temp.length);
            imageView.setImageBitmap(photo);
        }
        holder.callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mContext = v.getContext();
                String str = name.getText().toString();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + str));
                mContext.startActivity(intent);
                Toast.makeText(mContext, "name" + position, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mlistCalls.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, duration, date;
        Button callButton;
        CircleImageView callLogImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.contact_name);
            duration = itemView.findViewById(R.id.call_duration);
            date = itemView.findViewById(R.id.call_date);
            callButton = itemView.findViewById(R.id.call_log_button);
            callLogImage = itemView.findViewById(R.id.call_log_image);
        }
    }
}


