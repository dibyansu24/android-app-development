package com.rohit.contactsapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.rohit.contactsapp.R;
import com.rohit.contactsapp.models.Modelsfordashboard;

import java.util.List;

public class sliding_cardAdapter extends PagerAdapter {
    private List<Modelsfordashboard> models;
    private LayoutInflater inflater;
    private Context context;

    public sliding_cardAdapter(List<Modelsfordashboard> models, Context context) {
        this.models = models;
        this.context = context;
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view.equals(o);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.card_view, container, false);

        TextView start_date, end_date;
        ProgressBar progressBar;

        start_date = view.findViewById(R.id.start_date);
        end_date = view.findViewById(R.id.end_date);
        progressBar = view.findViewById(R.id.progress_bar);

        start_date.setText(models.get(position).getDate());
        end_date.setText(models.get(position).getEndDate());
        progressBar.setProgress(models.get(position).getProgress());

        container.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}