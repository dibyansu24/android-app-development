package com.rohit.contactsapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.rohit.contactsapp.R;
import com.rohit.contactsapp.models.EmailsResponses.EntryList;

import java.util.List;


public class EmailListAdapter extends RecyclerView.Adapter<EmailListAdapter.EmailViewHolder> {

    Context context;
    List<EntryList> emailListResponses;

    public EmailListAdapter(Context context, List<EntryList> emailListResponses){
     this.context = context;
     this.emailListResponses = emailListResponses;
    }

    @NonNull
    @Override
    public EmailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view  = LayoutInflater.from(context).inflate(R.layout.activity_fetch_emails,parent,false);

        return new EmailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmailViewHolder holder, int position) {
        EntryList res = emailListResponses.get(position);

        holder.title.setText(emailListResponses.get(position).getNameValueList().getName().getValue());
//        holder.status.setText(res.getNameValueList().getStatus().getValue());
        holder.date.setText(res.getNameValueList().getDateEntered().getValue());

    }

    @Override
    public int getItemCount() {
        return emailListResponses.size();
    }

    public class EmailViewHolder extends RecyclerView.ViewHolder {

       public AppCompatTextView title,status,date;

        public EmailViewHolder(@NonNull View itemView) {
            super(itemView);

            title = (AppCompatTextView)itemView.findViewById(R.id.title_tv);
            status = (AppCompatTextView)itemView.findViewById(R.id.status_tv);
            date = (AppCompatTextView)itemView.findViewById(R.id.date_tv);

        }
    }
}