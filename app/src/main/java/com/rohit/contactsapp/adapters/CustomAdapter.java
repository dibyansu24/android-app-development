package com.rohit.contactsapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.rohit.contactsapp.Classes.ContactInfo;
import com.rohit.contactsapp.R;

import java.util.List;

public class CustomAdapter<C> extends ArrayAdapter {

    private List contactInfoList;
    private Context context;

    public CustomAdapter(Context context, int list_item, int resource, List objects) {
        super(context, resource, objects);
        this.contactInfoList = objects;
        this.context = context;
    }

    private class ViewHolder {
        TextView displayName;
        //TextView phoneNumber;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item, null);
            holder = new ViewHolder();
            holder.displayName = (TextView) convertView.findViewById(R.id.list_desc);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ContactInfo contactInfo = (ContactInfo) contactInfoList.get(position);
        holder.displayName.setText(contactInfo.getDisplayName());
        return convertView;
    }
}
