package com.rohit.contactsapp.Activities.UserGuide;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.rohit.contactsapp.R;

public class HelpAndFeedback extends AppCompatActivity {
    TextView  textview1 , textview2 , textview3 ,  textview4 , textview5 , textview6 , textview7, textview8 , textview9 , textview10;
    ImageButton button , button1 , button2 , button3 , button4 , button5 , button6 , button7;
    ImageView icon , icon1 , icon2 , icon3 , icon4 , icon5, icon6;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_and_feedback);

        textview1 = findViewById(R.id.textview1);
        textview2 = findViewById(R.id.textview2);
        textview3 = findViewById(R.id.textview3);
        textview4 = findViewById(R.id.textview4);
        textview5 = findViewById(R.id.textview5);
        textview6 = findViewById(R.id.textview6);
        textview7 = findViewById(R.id.textview7);
        textview8 = findViewById(R.id.textview8);
        textview9 = findViewById(R.id.textview9);
        textview10 = findViewById(R.id.textview10);


        Toolbar toolbar = findViewById(R.id.toolbar_help);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24); // your drawable

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ImageButton button = (ImageButton)findViewById(R.id.button);

        final Context context = this;
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(context, UserGuide.class);
                startActivity(intent);
            }
        });



        ImageButton button1 = (ImageButton) findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ReportProblem.class);
                startActivity(intent);
            }

        });



        ImageButton button2 = (ImageButton) findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WriteReview.class);
                startActivity(intent);
            }

        });



        ImageButton button3 = (ImageButton) findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SendSuggestion.class);
                startActivity(intent);
            }

        });




        ImageButton button4 = (ImageButton) findViewById(R.id.button4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, licences.class);
                startActivity(intent);
            }

        });

        ImageButton button5 = (ImageButton) findViewById(R.id.button5);
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TermsService.class);
                startActivity(intent);
            }

        });



        ImageButton button6 = (ImageButton) findViewById(R.id.button6);
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PrivacyPolicy.class);
                startActivity(intent);
            }

        });


        ImageButton button7 = (ImageButton) findViewById(R.id.button7);
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, HelpAndFeedback1.class);
                startActivity(intent);
            }

        });


        ImageView icon = findViewById(R.id.icon);
        ImageView icon1 = findViewById(R.id.icon1);
        ImageView icon2 = findViewById(R.id.icon2);
        ImageView icon3 = findViewById(R.id.icon3);
        ImageView icon4 = findViewById(R.id.icon4);
        ImageView icon5 = findViewById(R.id.icon5);
        ImageView icon6 = findViewById(R.id.icon6);

    }


}