package com.rohit.contactsapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.rohit.contactsapp.Activities.UserGuide.MainAdapter;
import com.rohit.contactsapp.R;

public class SocialApp extends AppCompatActivity {
    GridView gridView;
    Toolbar toolbar;
    String[] numberword={"GOOGLE","GOOGLE DRIVE","GMAIL","GOOGLE PLUS","DROPBOX","YAHOO","FACEBOOK","YOUTUBE","TWITTER","OUTLOOK","SKYPE","LINKEDIN","INSTAGRAM","WHATSAPP","SNAPCHAT","PINTEREST","REDDIT","QUORA"};

    int[] numberImage ={R.mipmap.ic_launcher ,  R.drawable.ic_facebook, R.drawable.google, R.drawable.google, R.drawable.google,R.drawable.google, R.drawable.google, R.drawable.google, R.drawable.google, R.drawable.google, R.drawable.google,R.drawable.linkedin, R.drawable.google, R.drawable.google, R.drawable.google, R.drawable.google, R.drawable.google, R.drawable.google};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_app);

         toolbar = (Toolbar) findViewById(R.id.social_toolbar);
         toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24); // your drawable
         setSupportActionBar(toolbar);
         getSupportActionBar().setDisplayHomeAsUpEnabled(true);
         getSupportActionBar().setDisplayShowHomeEnabled(true);


        gridView=findViewById(R.id.grid_view);
        final MainAdapter adapter = new MainAdapter(SocialApp.this,numberword,numberImage);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        Uri uri =Uri.parse("http://www.google.com");
                        Intent intent = new Intent(Intent.ACTION_VIEW , uri);
                        startActivity(intent);
                        break;

                    case 1:
                        Uri uri1 =Uri.parse("http://www.drive.google.com");
                        Intent intent1 = new Intent(Intent.ACTION_VIEW , uri1);
                        startActivity(intent1);
                        break;
                    case 2:
                        Uri uri2 =Uri.parse("http://www.gmail.com");
                        Intent intent2 = new Intent(Intent.ACTION_VIEW , uri2);
                        startActivity(intent2);
                        break;
                    case 3:
                        Uri uri3 =Uri.parse("http://www.plus.google.com");
                        Intent intent3 = new Intent(Intent.ACTION_VIEW , uri3);
                        startActivity(intent3);
                        break;
                    case 4:
                        Uri uri4 =Uri.parse("http://www.dropbox.com");
                        Intent intent4= new Intent(Intent.ACTION_VIEW , uri4);
                        startActivity(intent4);
                        break;
                    case 5:
                        Uri uri5 =Uri.parse("http://www.yahoo.com");
                        Intent intent5 = new Intent(Intent.ACTION_VIEW , uri5);
                        startActivity(intent5);
                        break;
                    case 6:
                        Uri uri6 =Uri.parse("http://www.facebook.com");
                        Intent intent6 = new Intent(Intent.ACTION_VIEW , uri6);
                        startActivity(intent6);
                        break;
                    case 7:
                        Uri uri7 =Uri.parse("http://www.youtube.com");
                        Intent intent7= new Intent(Intent.ACTION_VIEW , uri7);
                        startActivity(intent7);
                        break;
                    case 8:
                        Uri uri8 =Uri.parse("http://www.twitter.com");
                        Intent intent8 = new Intent(Intent.ACTION_VIEW , uri8);
                        startActivity(intent8);
                        break;
                    case 9:
                        Uri uri9 =Uri.parse("http://www.outlook.com");
                        Intent intent9 = new Intent(Intent.ACTION_VIEW , uri9);
                        startActivity(intent9);
                        break;
                    case 10:
                        Uri uri10 =Uri.parse("http://www.skype.com");
                        Intent intent10 = new Intent(Intent.ACTION_VIEW , uri10);
                        startActivity(intent10);
                        break;
                    case 11:
                        Uri uri11 =Uri.parse("http://www.linkdin.com");
                        Intent intent11 = new Intent(Intent.ACTION_VIEW , uri11);
                        startActivity(intent11);
                        break;
                    case 12:
                        Uri uri12 =Uri.parse("http://www.instagram.com");
                        Intent intent12 = new Intent(Intent.ACTION_VIEW , uri12);
                        startActivity(intent12);
                        break;

                    case 13:
                        Uri uri13 =Uri.parse("https://api.whatsapp.com/send?phone=50600000000");
                        Intent intent13 = new Intent(Intent.ACTION_VIEW , uri13);
                        startActivity(intent13);
                        break;

                    case 14:
                        Uri uri14 =Uri.parse("http://www.snapchat.com");
                        Intent intent14 = new Intent(Intent.ACTION_VIEW , uri14);
                        startActivity(intent14);
                        break;
                    case 15:
                        Uri uri15=Uri.parse("http://www.in.pinterest.com");
                        Intent intent15= new Intent(Intent.ACTION_VIEW , uri15);
                        startActivity(intent15);
                        break;

                    case 16:
                        Uri uri16 =Uri.parse("http://www.reddit.com");
                        Intent intent16 = new Intent(Intent.ACTION_VIEW , uri16);
                        startActivity(intent16);
                        break;
                    case 17:
                        Uri uri17 =Uri.parse("http://www.quora.com");
                        Intent intent17 = new Intent(Intent.ACTION_VIEW , uri17);
                        startActivity(intent17);
                        break;
                }
            }
        });


    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}