package com.rohit.contactsapp.Activities.UserGuide;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.rohit.contactsapp.R;

public class HelpAndFeedback1 extends AppCompatActivity {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_feedback);
        text = findViewById(R.id.text);
    }
}