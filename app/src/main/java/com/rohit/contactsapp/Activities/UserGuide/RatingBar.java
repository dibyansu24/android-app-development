package com.rohit.contactsapp.Activities.UserGuide;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.rohit.contactsapp.R;

public class RatingBar extends AppCompatActivity implements android.widget.RatingBar.OnRatingBarChangeListener{

    private android.widget.RatingBar rBar;
    private TextView tView , tview1 , tview2;

    private Button btn , btn1;
    private android.widget.SeekBar seekBar;
    private Object SeekBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_bar);
        rBar = (android.widget.RatingBar) findViewById(R.id.ratingbar);
        rBar.setOnRatingBarChangeListener((android.widget.RatingBar.OnRatingBarChangeListener) this);
        tView = (TextView) findViewById(R.id.text);
        tview1 = (TextView) findViewById(R.id.text1);
        tview2 = (TextView) findViewById(R.id.text2);
        btn = (Button) findViewById(R.id.btnGet);
        btn1 = (Button) findViewById(R.id.btnGet1);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int noofstars = rBar.getNumStars();
                float getrating = rBar.getRating();
                tView.setText("Rating: " + getrating + "/" + noofstars);
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int noofstars = rBar.getNumStars();
                float getrating = rBar.getRating();
                tView.setText("Rating: " + getrating + "/" + noofstars);
            }
        });

    }

    @Override

    public void onRatingChanged(android.widget.RatingBar ratingBar, float rating,
                                boolean fromUser) {
        Toast.makeText(RatingBar.this, "New Rating: " + rating,
                Toast.LENGTH_SHORT).show();
    }
}