package com.rohit.contactsapp.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.rohit.contactsapp.R;
import com.rohit.contactsapp.adapters.Coustom_Adapter2;
import com.rohit.contactsapp.interfaces.ApiInterface;
import com.rohit.contactsapp.models.Models;
import com.rohit.contactsapp.models.Modules;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CoustomModulename extends AppCompatActivity {

    public ArrayList<Modules> modules;
    Coustom_Adapter2 adapter;
    ArrayList<String> selectedModules = new ArrayList<>();
    private ListView listView;
    CheckBox checkBox;
    Navigation_Drawer navigation_drawer;
    int position[];
    TextView rohit;
    ProgressDialog pd;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coustom_modulename);

        pd = new ProgressDialog(CoustomModulename.this);
        pd.setMessage("loading");
        pd.show();

        /////////    find View By ids
        listView = findViewById(R.id.allModulesname);
        toolbar = (Toolbar) findViewById(R.id.toolbar_id);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24); // your drawable

         setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // back button pressed
                openNewActivity();

            }
        });
         getSupportActionBar().setDisplayHomeAsUpEnabled(true);
         getSupportActionBar().setDisplayShowHomeEnabled(true);

        getModuleList();
//        Button back = findViewById(R.id.back);
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                openNewActivity();
//
//            }
//        });
    }

    public void openNewActivity() {
        Intent intent = new Intent(CoustomModulename.this, Navigation_Drawer.class);

            intent.putExtra("key", adapter.sendCoustom_module);
            setResult(2,intent);
            startActivity(intent);
    }

    public void getModuleList() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://uat.ideadunes.com/projects/devs/testapi_giproperties/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<Models> call = apiInterface.getModels();
        call.enqueue(new Callback<Models>() {
            @Override
            public void onResponse(Call<Models> call, Response<Models> response) {
                if (response.isSuccessful()) {
                    modules = response.body().getModules();
                    adapter = new Coustom_Adapter2(getApplicationContext(), modules, R.layout.listof);
                    listView.setAdapter(adapter);

                    pd.dismiss();

                }
            }

            @Override
            public void onFailure(Call<Models> call, Throwable t) {
            }
        });
    }
}