package com.rohit.contactsapp.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.rohit.contactsapp.R;

public class GroupActivity extends AppCompatActivity {
    TextView CreateGroup;
    EditText GroupName;
    Button CancelButton,OKButton;
    LinearLayout linearLayout1,linearLayout2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groupactivity);

                CreateGroup = findViewById(R.id.create_group);
                GroupName = findViewById(R.id.group_name);
                CancelButton = findViewById(R.id.Cancel_button);
                OKButton = findViewById(R.id.ok_button);
                linearLayout1 = findViewById(R.id.linear1);
                linearLayout2 = findViewById(R.id.linear2);

                FloatingActionButton floatingActionButton =
                        findViewById(R.id.fab);

                floatingActionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CreateGroup.setVisibility(View.VISIBLE);
                        GroupName.setVisibility(View.VISIBLE);
                        linearLayout2.setVisibility(View.VISIBLE);
                        linearLayout1.setVisibility(View.VISIBLE);
                    }
                });
            }
        }
