package com.rohit.contactsapp.Activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import com.rohit.contactsapp.R;
import com.rohit.contactsapp.adapters.ContactViewPagerAdapter;
import com.rohit.contactsapp.adapters.ViewPagerAdapter;
import com.rohit.contactsapp.fragments.FragmentCRMCalls;
import com.rohit.contactsapp.fragments.FragmentCalls;

public class CallLogActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;


    private void configureView () {




        tabLayout=(TabLayout) findViewById(R.id.tab_layout);
        viewPager=(ViewPager) findViewById(R.id.view_pager);

        tabLayout.addTab(tabLayout.newTab().setText("calllog"));
        tabLayout.addTab(tabLayout.newTab().setText("crm call log"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(this, getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pagerAdapter);


        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

      //  askpermissions();

        configureView();
    }
    }
