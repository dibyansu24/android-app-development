package com.rohit.contactsapp.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.rohit.contactsapp.R;

public class CreateNewContact extends AppCompatActivity {

    ImageButton camera;
    Spinner spinner1,spinner2,spinner3;
    AlertDialog.Builder builder;
    Button GroupNameButton;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_contact);


                GroupNameButton = findViewById(R.id.group_name);
                spinner1 = (Spinner) findViewById(R.id.spinner1);

                ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.planets_array1, android.R.layout.simple_spinner_item);

                adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


                spinner1.setAdapter(adapter1);

                spinner1.setPrompt("home");

                spinner2 = (Spinner) findViewById(R.id.spinner2);

                ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                        R.array.planets_array2, android.R.layout.simple_spinner_item);

                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spinner2.setAdapter(adapter2);
                spinner2.setPrompt("email");

                spinner3 = (Spinner) findViewById(R.id.spinner3);
                ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(this,
                        R.array.planets_array3, android.R.layout.simple_spinner_item);
                adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spinner3.setAdapter(adapter3);
                spinner3.setPrompt("home");
                GroupNameButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent GroupIntent = new Intent(CreateNewContact.this, GroupActivity.class);
                        startActivity(GroupIntent);
                    }
                });


            }
            public void showAlertDialogButtonClicked(View view) {

                // setup the alert builder
                builder = new AlertDialog.Builder(this);
                builder.setTitle("Add another Field");

                // add a list
                String[] animals = {"Phonetic name", "IM", "Address", "Nickname", "Website","Internet Call","Birthday","Notes"};
                builder.setItems(animals, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {
                        switch (position) {
                            case 0:
                                Toast.makeText(CreateNewContact.this, "position"+position, Toast.LENGTH_SHORT).show();
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                        }
                    }
                });

                // create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }


       // camera = findViewById(R.id.camera_image);

//        camera.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(CreateNewContact.this,"Entered in On ClickListener",Toast.LENGTH_LONG).show();
//                Intent mintent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivity(mintent);
//            }
//        });
//        Spinner spinner1 = (Spinner) findViewById(R.id.spinner1);
//// Create an ArrayAdapter using the string array and a default spinner layout
//        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.planets_array1, android.R.layout.simple_spinner_item);
//// Specify the layout to use when the list of choices appears
//        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//// Apply the adapter to the spinner
//        spinner1.setAdapter(adapter1);
//
//        Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);
//// Create an ArrayAdapter using the string array and a default spinner layout
//        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.planets_array2, android.R.layout.simple_spinner_item);
//// Specify the layout to use when the list of choices appears
//        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//// Apply the adapter to the spinner
//        spinner2.setAdapter(adapter2);
//
//        Spinner spinner3 = (Spinner) findViewById(R.id.spinner3);
//// Create an ArrayAdapter using the string array and a default spinner layout
//        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(this, R.array.planets_array3, android.R.layout.simple_spinner_item);
//// Specify the layout to use when the list of choices appears
//        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//// Apply the adapter to the spinner
//        spinner3.setAdapter(adapter3);
//
//        Spinner spinner4 = (Spinner) findViewById(R.id.spinner4);
//// Create an ArrayAdapter using the string array and a default spinner layout
//        ArrayAdapter<CharSequence> adapter4 = ArrayAdapter.createFromResource(this,
//                R.array.planets_array4, android.R.layout.simple_spinner_item);
//// Specify the layout to use when the list of choices appears
//        adapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//// Apply the adapter to the spinner
//        spinner4.setAdapter(adapter4);
