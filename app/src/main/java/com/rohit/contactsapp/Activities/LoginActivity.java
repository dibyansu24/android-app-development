package com.rohit.contactsapp.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.rohit.contactsapp.Network.ServiceGenerator;
import com.rohit.contactsapp.R;
import com.rohit.contactsapp.models.LoginResponsess.LoginResponse;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText mail_et, pass_et;
    String email;
    String password;
    Button button_login;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    private String TAG = "login";
    private GoogleSignInClient mGoogleSignInClient;
    public String SESSION = "id";
    public String USER_ID = "user_id";
    public String USER_NAME = "user_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
      //  askpermissions();

        mail_et = findViewById(R.id.mail);
        pass_et = findViewById(R.id.password);
        button_login = findViewById(R.id.login);

//        sharedpreferences =  PreferenceManager.getDefaultSharedPreferences(this);
        button_login = findViewById(R.id.login_btn);

        sharedpreferences = getSharedPreferences("Contact App", Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                validate();
            }
        });
    }
    public void validate() {
//        email = mail_et.getText().toString();
//        password = pass_et.getText().toString();
        email = "webapp-api@ideadunes.com";
        password = "Password123";

        if (email.isEmpty()) {
            Toast.makeText(this, "enter email", Toast.LENGTH_LONG).show();
        } else if (password.isEmpty()) {
            Toast.makeText(this, "enter password", Toast.LENGTH_LONG).show();
        }

        login();
    }

    public void login() {

        String userAuthString =
                "{\"user_auth\" " +
                        ":{\"user_name\":\"webapp-api@ideadunes.com\",\"password\":\"Password123\",\"encryption\":\"PLAIN\"}" +
                ",\"application\":\"MyRestAPI\"}";

        try {
            JSONObject jObject = new JSONObject(userAuthString);
            Log.i(TAG, "login:\n"+jObject);
        }
        catch (Exception err){
            Log.i(TAG, "login: "+"error");
        }
        Map<String, Object> body = new HashMap<>();
        body.put("input_type", "JSON");
        body.put("response_type", "JSON");
        body.put("method", "login");
        body.put("rest_data", userAuthString);

        ServiceGenerator.getApi().logIn(body).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                LoginResponse loginRespons = response.body();
                if (response.code() == 200) {

                    Toast.makeText(LoginActivity.this, "Login Successfully", Toast.LENGTH_SHORT).show();

                    editor.putString(SESSION, "" + response.body().getId());
                    editor.putString(USER_ID, "" + response.body().getNameValueList().getUserId().getValue());
                    editor.putString(USER_NAME, "" + response.body().getNameValueList().getUserName().getValue());

                    Log.i(TAG, "onSuccess: " + response.body().getId()
                            +'\n'+ response.body().getNameValueList().getUserName().getValue()
                            +'\n'+ response.body().getNameValueList().getUserName().getName());

                    editor.commit();

                    startActivity(new Intent(LoginActivity.this, DashboardActivity.class));

                } else if (!response.isSuccessful()) {
                    Log.e(TAG, "onSuccess: " + response.code() + "\n"+ response.message());

                }
            }
            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e(TAG, "onSuccess: " + t.getMessage());


            }
        });


    }

//    private void askpermissions(){
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG)!= PackageManager.PERMISSION_GRANTED){
//            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_CONTACTS},1);
//            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_CALL_LOG},1);
//        }
//    }
}

      /*  GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        signInButton = findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
<<<<<<< HEAD
	//hello
=======

>>>>>>> 48a40b32b79766130c7e709cac05bb0a14fab2b1


        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });




        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mCallManager = CallbackManager.Factory.create();
        mLoginButton = findViewById(R.id.login_button);
        printHashKey(getApplicationContext());

        mLoginButton.registerCallback(mCallManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();

                Log.e(TAG, "onSuccess: " + accessToken.getUserId());

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException exception) {
                exception.printStackTrace();
                Log.e(TAG, "onError: " + exception.getMessage());
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        mCallManager.onActivityResult(requestCode, resultCode, data);




        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }

    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);


            Intent intent = new Intent(login.this,home.class);
            startActivity(intent);

        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    public static void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.e("login", "printHashKey: " + hashKey);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }


}*/