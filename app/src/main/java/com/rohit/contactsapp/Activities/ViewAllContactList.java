//package com.rohit.contactsapp.Activities;
//
//import androidx.annotation.NonNull;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.core.app.ActivityCompat;
//
//import android.annotation.TargetApi;
//import android.content.ContentResolver;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.database.Cursor;
//import android.os.Build;
//import android.os.Bundle;
//import android.provider.ContactsContract;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.ListView;
//import android.widget.Toast;
//
//import com.rohit.contactsapp.Classes.ContactInfo;
//import com.rohit.contactsapp.R;
//import com.rohit.contactsapp.adapters.CustomAdapter;
//
//import java.util.ArrayList;
//
//public class ViewAllContactList extends AppCompatActivity {
//    public static  final int  REQUEST_READ_CONTACTS =  79;
//
//    ListView list;
//    ArrayList<ContactInfo> contactsInfoList;
//
//    //difference bet LOLLIPOP AND M
//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//
//    @TargetApi(Build.VERSION_CODES.M)
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_view_all_contact_list);
//
//        list = findViewById(R.id.list_view);
//
//        if (ActivityCompat.checkSelfPermission(this,android.Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
//            contactsInfoList = getAllContacts();
//        }else {
//            requestPermission();
//        }
//
//        CustomAdapter<ContactInfo> adapter = new CustomAdapter<>(this,R.layout.list_item,R.id.list_desc,contactsInfoList);
//        list.setAdapter(adapter);
//
//        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                ContactInfo contact = (ContactInfo) parent.getItemAtPosition(position);
//
//                //String contactId = contact.getContactId();
//                String contactNumber = contact.getPhoneNumber();
//                String contactName = contact.getDisplayName();
//
//
//                Intent intent = new Intent(ViewAllContactList.this, ContactsDetailsActivity.class);
//                intent.putExtra("contactPerson", contactName);
//                intent.putExtra("contactNo",contactNumber);
//                //intent.putExtra("contactId",contactId);
//
//                Toast.makeText(ViewAllContactList.this, "Select Contact"+position, Toast.LENGTH_SHORT).show();
//                startActivity(intent);
//            }
//        });
//    }
//
//    private  void  requestPermission() {
//        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_CONTACTS)) {
//            // show UI part if you want here to show some rationale !!!
//
//        } else {
//            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_CONTACTS}, REQUEST_READ_CONTACTS);
//
//        }
//        if (ActivityCompat.shouldShowRequestPermissionRationale(this,android.Manifest.permission.READ_CONTACTS)) {
//
//        } else {
//            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_CONTACTS}, REQUEST_READ_CONTACTS);
//        }
//    }
//
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
//        if (requestCode == REQUEST_READ_CONTACTS) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                contactsInfoList = getAllContacts();
//                Toast.makeText(this, " All contact List are show ", Toast.LENGTH_SHORT).show();
//            } else {
//                Toast.makeText(this, "Cotact List Are Not Found ", Toast.LENGTH_SHORT).show();
//            }
//            return ;
//        }
//    }
//
//    private ArrayList getAllContacts() {
//        ContentResolver cr = getContentResolver();
//        contactsInfoList = new ArrayList<ContactInfo>();
//
//        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI,null,null,null,null);
//        if ((cursor != null ?  cursor.getCount() : 0) > 0) {
//            while (cursor.moveToNext()) {
//                ContactInfo contactInfo = new ContactInfo();
//                String id  = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
//                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
//
//                contactInfo.setDisplayName(name);
//                contactInfo.setContactId(id);
//
//                if (cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))>0){
//                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
//                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
//                            new  String[]{id}, null);
//                    while (pCur.moveToNext()) {
//                        String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                        contactInfo.setPhoneNumber(phoneNo);
//
//                    }
//                    pCur.close();
//
//                    contactsInfoList.add(contactInfo);
//                } else  {
//                    System.out.println("No show contact list");
//                }
//            }
//        }
//        if (cursor != null){
//            cursor.close();
//        }
//        return contactsInfoList;
//    }
//}