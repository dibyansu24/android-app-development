package com.rohit.contactsapp.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.rohit.contactsapp.R;
import com.rohit.contactsapp.adapters.Coustom_Adapter2;
import com.rohit.contactsapp.adapters.sliding_cardAdapter;
import com.rohit.contactsapp.adapters.FeedAdapter;
import com.rohit.contactsapp.models.Modelsfordashboard;
import com.rohit.contactsapp.models.Modelsforfeeds;
import com.rohit.contactsapp.models.Modules;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends AppCompatActivity {

    public static View.OnClickListener myOnClickListener;
    private static ArrayList<Modelsforfeeds> data;
    public ViewPager pager;
    public ArrayList<Modules> modules;
    androidx.appcompat.widget.Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    ArrayList<String> str = new ArrayList<String>();
    Coustom_Adapter2 adapter4;
    TextView coustomModules;
    ListView listView;
    Button refresh;
    sliding_cardAdapter adapter;
    List<Modelsfordashboard> models;
    FeedAdapter adapter2;
    Button navigation_drawer;
    ListView expandableListView;
    Coustom_Adapter2 adapter3;
    RecyclerView recyclerView;
    DrawerLayout drawerLayout;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);


        //expandableListView = findViewById(R.id.exoandable_list);
        listView = findViewById(R.id.listview);
        refresh = findViewById(R.id.refresh);
        coustomModules = findViewById(R.id.CoustomModule);
        recyclerView = findViewById(R.id.feed_recycle);
        navigation_drawer = findViewById(R.id.navigation_drawer);
        drawerLayout = findViewById(R.id.drawer_layout);
        pager = findViewById(R.id.todo_pager);


        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listView != null)
                    addData();
                else
                    Toast.makeText(DashboardActivity.this, "Select Modules", Toast.LENGTH_SHORT).show();
            }
        });

        str.add("Home");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                str);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //  Intent intent = new Intent(Navigation_Drawer.this, Email_Activity.class);
                // startActivity(intent);
            }
        });
        coustomModules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, CoustomModulename.class);
                startActivityForResult(intent, 2);
            }
        });
        //  menu_item();


        //myOnClickListener = new MyOnClickListener(this);

        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        navigation_drawer.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                if (!drawerLayout.isDrawerOpen(Gravity.START))
                    drawerLayout.openDrawer(Gravity.START);
                else drawerLayout.closeDrawer(Gravity.END);
            }
        });


        models = new ArrayList<>();
        models.add(new Modelsfordashboard("Start date: 02/15/5656", "end date: 02155", 70));

        models.add(new Modelsfordashboard("Start date: 02/15/5656", "end date: 1645", 0));

        models.add(new Modelsfordashboard("Start date: 02/15/5656", "end date: 56562", 0));

        adapter = new sliding_cardAdapter(models, this);
        pager.setAdapter(adapter);


        data = new ArrayList<>();
        data.add(new Modelsforfeeds(0, "Suraj", "12 jan 2102", "Ha ha Haq"));

        data.add(new Modelsforfeeds(0, "Rohit", "12 jan 2102", "I am good boy"));
        data.add(new Modelsforfeeds(0, "Rohan", "12 jan 2102", "You are stupid"));
        data.add(new Modelsforfeeds(0, "Unnamed", "12 jan 2102", "I am Rohit Kumar"));
        data.add(new Modelsforfeeds(0, "Suraj", "12 jan 2102", "Ha ha Haq"));

        data.add(new Modelsforfeeds(0, "Rohit", "12 jan 2102", "I am good boy"));
        data.add(new Modelsforfeeds(0, "Rohan", "12 jan 2102", "You are stupid"));
        data.add(new Modelsforfeeds(0, "Unnamed", "12 jan 2102", "I am Rohit Kumar"));
        data.add(new Modelsforfeeds(0, "Suraj", "12 jan 2102", "Ha ha Haq"));

        data.add(new Modelsforfeeds(0, "Rohit", "12 jan 2102", "I am good boy"));
        data.add(new Modelsforfeeds(0, "Rohan", "12 jan 2102", "You are stupid"));
        data.add(new Modelsforfeeds(0, "Unnamed", "12 jan 2102", "I am Rohit Kumar"));
        adapter2 = new FeedAdapter(data, this);
        recyclerView.setAdapter(adapter2);


    }


    public void addData() {

        str = (ArrayList<String>) getIntent().getSerializableExtra("key");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                str);
        listView.setAdapter(arrayAdapter);


    }

   /* public void menu_item() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://uat.ideadunes.com/projects/devs/testapi_giproperties/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        Call<Models> call = apiInterface.getModels();
        call.enqueue(new Callback<Models>() {
            @Override
            public void onResponse(Call<Models> call, Response<Models> response) {
                if (response.isSuccessful()) {
                    modules = response.body().getModules();
                    adapter3 = new Coustom_Adapter2(getApplicationContext(), modules, R.layout.listof);
                    listView.setAdapter(adapter3);

                }
            }

            @Override
            public void onFailure(Call<Models> call, Throwable t) {
            }
        });
    }

*/
}