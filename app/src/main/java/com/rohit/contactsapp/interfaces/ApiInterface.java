package com.rohit.contactsapp.interfaces;

import com.rohit.contactsapp.models.Models;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("modules.php")
    Call<Models> getModels();
}
